############################################################
############################################################
## this script checks for each translation file (*.po)    ##
## the translation progress and output the result as JSON ##
############################################################
############################################################

###############################################
# check if all required programms are present #
###############################################

if command -v jq &> /dev/null
then
    echo "Using $(jq --version | head -1)."
else
    echo "Could not find jq"
    exit 1
fi

if command -v msgmerge &> /dev/null
then
    echo "Using $(msgmerge --version | head -1)."
else
    echo "Could not find msgmerge"
    exit 1
fi


###########################
# define path to po files #
###########################

if [ -z ${PATH_TO_PO} ]
then
    PATH_TO_PO="po"
fi
echo "Using PATH_TO_PO = \"${PATH_TO_PO}\""

###################
# define POT file #
###################

if [ -z ${POT_FILE} ]
then
    POT_FILE="${FULL_PROJECT_NAME}.pot"
fi
echo "Using POT_FILE = \"${PATH_TO_PO}/${POT_FILE}\""


#######################
# define results file #
#######################

if [ -z ${RESULTS_FILE} ]
then
    RESULTS_FILE="$(pwd)/translation-status.json"
fi
echo "Using RESULTS_FILE = \"${RESULTS_FILE}\""

# make sure, RESULTS_FILE is an empty JSON
echo "{}" > ${RESULTS_FILE}

# go into PO directory and loop over all .po files
echo ""
echo "parsing .po files ..."
pushd ${PATH_TO_PO} > /dev/null || exit 1
for f in $(find -name "*.po");
do
    # extract language code
    LANG=${f%.*}
    LANG=${LANG#*/}

    echo "--- parse \"${LANG}\" in file \"${f}\""

    # ensure, POT file exists
    if [ ! -f ${POT_FILE} ]
    then
        echo "[ERROR] Could not find POT file \"${POT_FILE}\" at PWD=\"$(pwd)\"."
        exit 2
    fi

    # generate updated .po file, only grep lines that are not commented by "#~"
    POFILE=$(msgmerge $f ${POT_FILE} 2> /dev/null | grep -v "#~")

    # compute total number of translations
    TOTALCOUNTS=$(echo -e "${POFILE}" | awk 'BEGIN { x=0 } /msgid/ { x++ } END { print x }' RS="\n\n")

    # compute number of empty translations
    COUNTS=$(echo -e "${POFILE}" | awk 'BEGIN { x=0 } $NF == "msgstr \"\"" { x++ } END { print x }' FS="\n" RS="\n\n")

    FILLED_COUNTS="$(($TOTALCOUNTS - $COUNTS))"
    PROGRESS=$( (( ${TOTALCOUNTS} > 0 )) && echo "$((100*${FILLED_COUNTS}/${TOTALCOUNTS}))%" || echo "0%")

    # write results to file
    jq ".${LANG}.total=${TOTALCOUNTS}" \
    ${RESULTS_FILE} > ${RESULTS_FILE}.tmp && mv ${RESULTS_FILE}.tmp ${RESULTS_FILE}

    jq ".${LANG}.empty=${COUNTS}" \
    ${RESULTS_FILE} > ${RESULTS_FILE}.tmp && mv ${RESULTS_FILE}.tmp ${RESULTS_FILE}

    jq ".${LANG}.filled=${FILLED_COUNTS}" \
    ${RESULTS_FILE} > ${RESULTS_FILE}.tmp && mv ${RESULTS_FILE}.tmp ${RESULTS_FILE}

    jq ".${LANG}.progress=\"${PROGRESS}\"" \
    ${RESULTS_FILE} > ${RESULTS_FILE}.tmp && mv ${RESULTS_FILE}.tmp ${RESULTS_FILE}
done
popd > /dev/null
echo "done."
echo ""


echo "The current translation process reads:"
jq "." ${RESULTS_FILE}
