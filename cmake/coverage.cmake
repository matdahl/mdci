# enable code coverage features for Debug builds
if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    message(STATUS "Add coverage compile flags ...")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --coverage")
else()
    message(STATUS "Build without coverage flags ...")
endif()

# output, which compiler CMake is using with which flags
message(STATUS "CMAKE_CXX_COMPILER_ID:      ${CMAKE_CXX_COMPILER_ID}")
message(STATUS "CMAKE_CXX_COMPILER_VERSION: ${CMAKE_CXX_COMPILER_VERSION}")
message(STATUS "CMAKE_CXX_FLAGS:            ${CMAKE_CXX_FLAGS}")
