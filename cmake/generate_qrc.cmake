# generates a QRC file at QRC_FILENAME with prefix QRC_PREFIX including all files that match ${QRC_SRC_DIR}/*${QRC_SRC_SUFFIX}
# QRC_SRC_SUFFIX can be a ";"-separated list of suffices
function(generate_qrc
        QRC_FILENAME
        QRC_SRC_DIR
        QRC_SRC_SUFFIX
        QRC_PREFIX)
    foreach(suffix ${QRC_SRC_SUFFIX})
        file(GLOB_RECURSE CUR_FILES
            RELATIVE ${QRC_SRC_DIR}
            ${QRC_SRC_DIR}/*${suffix})
        set(QRC_FILES "${QRC_FILES};${CUR_FILES}")
    endforeach()
    list(SORT QRC_FILES)
    execute_process(
        COMMAND bash -c "pushd ${QRC_SRC_DIR} > /dev/null; \
                         echo \"<RCC>\" > ${QRC_FILENAME}; \
                         echo \"  <qresource prefix=\\\"/${QRC_PREFIX}\\\">\" >> ${QRC_FILENAME}; \
                         for f in $(echo \"${QRC_FILES}\" | sed 's/;/ /g'); \
                         do \
                             echo \"    <file>$f</file>\" >> ${QRC_FILENAME}; \
                         done; \
                         echo \"  </qresource>\" >> ${QRC_FILENAME}; \
                         echo \"</RCC>\" >> ${QRC_FILENAME}; \
                         popd > /dev/null"
    )
    message(STATUS "Created QRC file at: ${QRC_FILENAME}")
endfunction()
