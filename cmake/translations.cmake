# collect all files where to look for translatable strings
file(GLOB_RECURSE I18N_SRC_FILES
    RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/po
    qml/*.qml
    qml/*.js
    src/*.cpp
    src/*.h
    plugins/*.cpp
    plugins/*.h
)
list(APPEND I18N_SRC_FILES
    ${DESKTOP_FILE_NAME}.in.h
)

if (MDTK_USE_CORE)
    message(STATUS "Include MDTK to translations ...")
    file(GLOB MDTK_I18N_FILES_CORE
        RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/po
        libs/mdtoolkit/qml/MDTK/*.qml
    )
    list(APPEND I18N_SRC_FILES ${MDTK_I18N_FILES_CORE})
endif()
if (MDTK_USE_EXPORTS)
    message(STATUS "Include MDTK.Exports to translations ...")
    file(GLOB MDTK_I18N_FILES_EXPORTS
        RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/po
        libs/mdtoolkit/qml/MDTK/Exports/*.qml
    )
    list(APPEND I18N_SRC_FILES ${MDTK_I18N_FILES_EXPORTS})
endif()
if (MDTK_USE_MANUAL)
    message(STATUS "Include MDTK.Manual to translations ...")
    file(GLOB MDTK_I18N_FILES_MANUAL
        RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/po
        libs/mdtoolkit/qml/MDTK/Manual/*.qml
    )
    list(APPEND I18N_SRC_FILES ${MDTK_I18N_FILES_MANUAL})
endif()
if (MDTK_USE_SETTINGS)
    message(STATUS "Include MDTK.Settings to translations ...")
    file(GLOB MDTK_I18N_FILES_SETTINGS
        RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/po
        libs/mdtoolkit/qml/MDTK/Settings/*.qml
    )
    list(APPEND I18N_SRC_FILES ${MDTK_I18N_FILES_SETTINGS})
endif()

find_program(INTLTOOL_MERGE intltool-merge)
if(NOT INTLTOOL_MERGE)
    message(FATAL_ERROR "Could not find intltool-merge, please install the intltool package")
endif()
find_program(INTLTOOL_EXTRACT intltool-extract)
if(NOT INTLTOOL_EXTRACT)
    message(FATAL_ERROR "Could not find intltool-extract, please install the intltool package")
endif()

add_custom_target(${DESKTOP_FILE_NAME} ALL
    COMMENT "Merging translations into ${DESKTOP_FILE_NAME}..."
    COMMAND LC_ALL=C ${INTLTOOL_MERGE} -d -u ${CMAKE_SOURCE_DIR}/po ${CMAKE_SOURCE_DIR}/${DESKTOP_FILE_NAME}.in ${DESKTOP_FILE_NAME}
    COMMAND sed -i 's/${PROJECT_NAME}-//g' ${CMAKE_BINARY_DIR}/${DESKTOP_FILE_NAME}
)
