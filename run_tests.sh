# runs the test suite including code coverage
# this script should be executed inside the clickable ide container

ARCH_TRIPLET="x86_64-linux-gnu"
BUILD_PATH="$(pwd)/build/${ARCH_TRIPLET}/app-Debug"

#################
# clean up data #
#################

find ${BUILD_PATH} -name '*.gcda' -exec rm {} \;
rm -f ${BUILD_PATH}/coverage/*.gcov


#################
# execute tests #
#################

for f in $(find ${BUILD_PATH}/tests -type f -executable);
do
    echo ""
    echo "Execute $f"
    echo "--------"
    $f
    echo ""
    echo ""
done


#########################
# generate gcov reports #
#########################

mkdir -p ${BUILD_PATH}/coverage
pushd ${BUILD_PATH}/coverage
find ${BUILD_PATH} -name '*.gcda' | xargs gcov > /dev/null
popd
